package com.twuc.webApp.web;

import com.twuc.webApp.contract.GetUserResponse;
import com.twuc.webApp.domain.UserEntity;
import com.twuc.webApp.domain.UserRepository;
import com.twuc.webApp.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {
    private UserService service;

    @GetMapping("/{id}")
    GetUserResponse getUser(@PathVariable long id){
        service = new UserService(new UserRepository());
        Optional<GetUserResponse> userResponse = service.getUser(id);
        if(userResponse.isPresent()){
            return userResponse.get();
        }
        else
            throw new IllegalArgumentException("id not exist or format error");
    }

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity handleIdException(){
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("id not exist or format error");
    }
}
