package com.twuc.webApp.domain;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
public class UserRepository {
    private List<UserEntity> storage = Arrays.asList(
        new UserEntity(1L, "Uzumaki", "Naruto"),
        new UserEntity(2L, "Edogawa", "Conan"),
        new UserEntity(3L, "Sinnjou", "Akane")
    );

    public Optional<UserEntity> findById(Long id) {
        return storage.stream().filter(u -> u.getId().equals(id)).findFirst();
    }

    public List<UserEntity> getStorage() {
        return storage;
    }
}
